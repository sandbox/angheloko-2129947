DESCRIPTION
-----------

This module defines the Commerce Merchant entity and related features. It allows
Commerce Product entities to be associated to merchant entities through an
entity reference field, which the module automatically creates. This
requirement, where products can be associated to different merchants, is a
common element in marketplace-like websites.



SUB-MODULES
-----------

* Commerce Merchant User

  This sub-module currently provides a user reference field to merchant entities
  that allows users to be associated to merchant entities.

  Upon installation, all existing merchant types will have a user reference
  field and any new merchant type will automatically have a user reference field
  upon saving.

* Commerce Merchant Product

  This sub-module currently provides a view that lists all products associated
  to a merchant. Only users associated to the merchant can access the view.

* Commerce Merchant Order

  This sub-module currently provides a view that lists all orders containing
  products associated to a merchant. Only users associated to the merchant can
  access the view.


INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.


CONFIGURATIONS
--------------

* Configure the product types that will have a merchant reference field:

  Users can select the product types that should have a merchant entityreference
  field by going to admin/commerce/config/merchant
