<?php

/**
 * @file
 * Contains admin related functions and forms.
 */

/**
 * Builds the merchant settings form.
 */
function commerce_merchant_ui_config_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['product_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product types'),
    '#description' => t('Enable the merchant field for the selected product types. Disabling the merchant field on a product type deletes the merchant data.'),
  );

  foreach (commerce_product_types() as $type => $product_type) {
    $instance[$type] = field_info_instance('commerce_product', COMMERCE_MERCHANT_UI_COMMERCE_MERCHANT_FIELD, $type);
    $enabled[$type] = (!empty($instance[$type]));

    $form['product_types'][$type] = array(
      '#type' => 'checkbox',
      '#default_value' => $enabled[$type],
      '#title' => t('@name', array('@name' => $product_type['name'])),
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#tree' => FALSE,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit handler for the merchant settings form.
 */
function commerce_merchant_ui_config_form_submit($form, &$form_state) {
  foreach ($form_state['values']['product_types'] as $type => $enable) {
    $instance = field_info_instance('commerce_product', COMMERCE_MERCHANT_UI_COMMERCE_MERCHANT_FIELD, $type);

    // If the product type doesn't have the field attached before and we need to
    // enable it now.
    if (empty($instance) && $enable) {
      // Create the instance.
      commerce_merchant_ui_configure_product_type($type);
      drupal_set_message(t('The merchant field has been added to the %type product type.', array('%type' => $type)));
    }
    // If the product type have the field attached before and we need to disable
    // it now.
    elseif (!empty($instance) && !$enable) {
      // Remove the instance.
      field_delete_instance($instance);
      drupal_set_message(t('The merchant field has been removed from %type product type', array('%type' => $type)));
    }
  }
}

/**
 * Configures the given product type.
 */
function commerce_merchant_ui_configure_product_type($type) {
  commerce_merchant_ui_create_instance($type);
}

/**
 * Creates a required, locked instance of a merchant field on the specified bundle.
 */
function commerce_merchant_ui_create_instance($bundle) {
  field_cache_clear();

  $field_name = COMMERCE_MERCHANT_UI_COMMERCE_MERCHANT_FIELD;
  $entity_type = 'commerce_product';
  $label = 'Merchant';

  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'entityreference',
      'cardinality' => 1,
      'entity_types' => array($entity_type),
      'translatable' => FALSE,
      'locked' => TRUE,

      'foreign keys' => array(
        'commerce_merchant' => array(
          'columns' => array(
            'target_id' => 'merchant_id',
          ),
          'table' => 'commerce_merchant',
        ),
      ),

      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'direction' => 'ASC',
            'property' => 'name',
            'type' => 'property',
          ),
          'target_bundles' => array(),
        ),
        'target_type' => 'commerce_merchant',
      ),
    );
    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,

      'label' => $label,
      'required' => TRUE,
      'settings' => array(),

      'widget' => array(
        'active' => 1,
        'settings' => array(
          'match_operator' => 'STARTS_WITH',
          'path' => '',
          'size' => 60,
        ),
        'type' => 'entityreference_autocomplete',
      ),

      'display' => array(),
    );
    field_create_instance($instance);
  }
}
