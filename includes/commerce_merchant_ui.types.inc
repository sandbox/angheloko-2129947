<?php

/**
 * @file
 * Contains merchant type CRUD forms.
 */

/**
 * Menu callback: display an overview of available types.
 */
function commerce_merchant_ui_types_overview() {
  drupal_add_css(drupal_get_path('module', 'commerce_merchant') . '/theme/commerce_merchant.admin.css');

  $header = array(
    t('Name'),
    t('Operations'),
  );

  $rows = array();

  // Loop through all defined merchant types.
  foreach (commerce_merchant_types() as $type => $merchant_type) {
    // Build the operation links for the current merchant type.
    $links = menu_contextual_links('commerce-merchant-type', 'admin/commerce/merchants/types', array(strtr($type, array('_' => '-'))));

    // Add the merchant type's row to the table's rows array.
    $rows[] = array(
      theme('merchant_type_admin_overview', array('merchant_type' => $merchant_type)),
      theme('links', array('links' => $links, 'attributes' => array('class' => 'links inline operations'))),
    );
  }

  // If no merchant types are defined...
  if (empty($rows)) {
    // Add a standard empty row with a link to add a new merchant type.
    $rows[] = array(
      array(
        'data' => t('There are no merchant types yet. <a href="@link">Add merchant type</a>.', array('@link' => url('admin/commerce/merchants/types/add'))),
        'colspan' => 2,
      ),
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Builds an overview of a merchant type for display to an administrator.
 */
function theme_merchant_type_admin_overview($variables) {
  $merchant_type = $variables['merchant_type'];

  $output = check_plain($merchant_type['name']);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $merchant_type['type'])) . '</small>';
  $output .= '<div class="description">' . filter_xss_admin($merchant_type['description']) . '</div>';

  return $output;
}

/**
 * Form callback wrapper: create or edit a merchant type.
 *
 * @param string $type
 *   The machine-name of the merchant type being created or edited by this form
 *   or a full merchant type array.
 *
 * @see commerce_merchant_merchant_type_form()
 */
function commerce_merchant_ui_merchant_type_form_wrapper($type) {
  if (is_array($type)) {
    $merchant_type = $type;
  }
  else {
    $merchant_type = commerce_merchant_type_load($type);
  }

  // Return a message if the merchant type is not governed by Merchant UI.
  if (!empty($merchant_type['type']) && $merchant_type['module'] != 'commerce_merchant_ui') {
    return t('This merchant type cannot be edited, because it is not defined by the Merchant UI module.');
  }

  // Include the forms file from the Merchant module.
  module_load_include('inc', 'commerce_merchant_ui', 'includes/commerce_merchant_ui.forms');

  return drupal_get_form('commerce_merchant_ui_merchant_type_form', $merchant_type);
}

/**
 * Form callback wrapper: confirmation form for deleting a merchant type.
 *
 * @param string $type
 *   The machine-name of the merchant type being created or edited by this form
 *   or a full merchant type array.
 *
 * @see commerce_merchant_merchant_type_delete_form()
 */
function commerce_merchant_ui_merchant_type_delete_form_wrapper($type) {
  if (is_array($type)) {
    $merchant_type = $type;
  }
  else {
    $merchant_type = commerce_merchant_type_load($type);
  }

  // Return a message if the merchant type is not governed by Merchant UI.
  if ($merchant_type['module'] != 'commerce_merchant_ui') {
    return t('This merchant type cannot be deleted, because it is not defined by the Merchant UI module.');
  }

  // Don't allow deletion of merchant types that have merchants already.
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'commerce_merchant', '=')
    ->entityCondition('bundle', $merchant_type['type'], '=')
    ->count();

  $count = $query->execute();

  if ($count > 0) {
    drupal_set_title(t('Cannot delete the %name merchant type', array('%name' => $merchant_type['name'])), PASS_THROUGH);

    return format_plural($count,
      'There is 1 merchant of this type. It cannot be deleted.',
      'There are @count merchants of this type. It cannot be deleted.'
    );
  }

  // Include the forms file from the Merchant module.
  module_load_include('inc', 'commerce_merchant_ui', 'includes/commerce_merchant_ui.forms');

  return drupal_get_form('commerce_merchant_ui_merchant_type_delete_form', $merchant_type);
}
