<?php

/**
 * @file
 * Forms for creating / editing and deleting merchants.
 */

/**
 * Form callback: create or edit a merchant type.
 */
function commerce_merchant_ui_merchant_type_form($form, &$form_state, $merchant_type) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_merchant_ui') . '/includes/commerce_merchant_ui.forms.inc';

  // Store the initial merchant type in the form state.
  $form_state['merchant_type'] = $merchant_type;

  $form['merchant_type'] = array(
    '#tree' => TRUE,
  );

  $form['merchant_type']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $merchant_type['name'],
    '#description' => t('The human-readable name of this merchant type. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#required' => TRUE,
    '#size' => 32,
  );

  if (empty($merchant_type['type'])) {
    $form['merchant_type']['type'] = array(
      '#type' => 'machine_name',
      '#title' => t('Machine name'),
      '#default_value' => $merchant_type['type'],
      '#maxlength' => 32,
      '#required' => TRUE,
      '#machine_name' => array(
        'exists' => 'commerce_merchant_type_load',
        'source' => array('merchant_type', 'name'),
      ),
      '#description' => t('The machine-readable name of this merchant type. This name must contain only lowercase letters, numbers, and underscores, it must be unique.'),
    );
  }

  $form['merchant_type']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Describe this merchant type. The text will be displayed on the <em>Add new content</em> page.'),
    '#default_value' => $merchant_type['description'],
    '#rows' => 3,
  );

  $form['merchant_type']['help'] = array(
    '#type' => 'textarea',
    '#title' => t('Explanation or submission guidelines'),
    '#description' => t('This text will be displayed at the top of the page when creating or editing merchants of this type.'),
    '#default_value' => $merchant_type['help'],
    '#rows' => 3,
  );

  $form['merchant_type']['revision'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default merchants of this type to be saved as new revisions when edited.'),
    '#default_value' => $merchant_type['revision'],
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 40,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save merchant type'),
    '#submit' => array_merge($submit, array('commerce_merchant_ui_merchant_type_form_submit')),
  );

  if (!empty($form_state['merchant_type']['type'])) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete merchant type'),
      '#suffix' => l(t('Cancel'), 'admin/commerce/merchants/types'),
      '#submit' => array_merge($submit, array('commerce_merchant_ui_merchant_type_form_delete_submit')),
      '#weight' => 45,
    );
  }
  else {
    $form['actions']['save_continue'] = array(
      '#type' => 'submit',
      '#value' => t('Save and add fields'),
      '#suffix' => l(t('Cancel'), 'admin/commerce/merchants/types'),
      '#submit' => array_merge($submit, array('commerce_merchant_ui_merchant_type_form_submit')),
      '#weight' => 45,
    );
  }

  $form['#validate'][] = 'commerce_merchant_ui_merchant_type_form_validate';

  return $form;
}

/**
 * Validation callback for commerce_merchant_merchant_type_form().
 */
function commerce_merchant_ui_merchant_type_form_validate($form, &$form_state) {
  $merchant_type = $form_state['merchant_type'];

  // If saving a new merchant type, ensure it has a unique machine name.
  if (empty($merchant_type['type'])) {
    if (!commerce_merchant_ui_validate_merchant_type_unique($form_state['values']['merchant_type']['type'])) {
      form_set_error('merchant_type][type', t('The machine name specified is already in use.'));
    }
  }
}

/**
 * Form submit handler: save a merchant type.
 */
function commerce_merchant_ui_merchant_type_form_submit($form, &$form_state) {
  $merchant_type = $form_state['merchant_type'];
  $updated = !empty($merchant_type['type']);

  // If a type is set, we should still check to see if a row for the type exists
  // in the database; this is done to accomodate types defined by Features.
  if ($updated) {
    $updated = db_query('SELECT 1 FROM {commerce_merchant_type} WHERE type = :type', array(':type' => $merchant_type['type']))->fetchField();
  }

  foreach ($form_state['values']['merchant_type'] as $key => $value) {
    $merchant_type[$key] = $value;
  }

  // Write the merchant type to the database.
  $merchant_type['is_new'] = !$updated;
  commerce_merchant_ui_merchant_type_save($merchant_type);

  // Redirect based on the button clicked.
  drupal_set_message(t('Merchant type saved.'));

  if ($form_state['triggering_element']['#parents'][0] == 'save_continue') {
    $form_state['redirect'] = 'admin/commerce/merchants/types/' . strtr($merchant_type['type'], '_', '-') . '/fields';
  }
  else {
    $form_state['redirect'] = 'admin/commerce/merchants/types';
  }
}

/**
 * Submit callback for delete button in commerce_merchant_ui_merchant_type_form().
 *
 * @see commerce_merchant_ui_merchant_type_form()
 */
function commerce_merchant_ui_merchant_type_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/commerce/merchants/types/' . strtr($form_state['merchant_type']['type'], '_', '-') . '/delete';
}

/**
 * Form callback: confirmation form for deleting a merchant type.
 */
function commerce_merchant_ui_merchant_type_delete_form($form, &$form_state, $merchant_type) {
  $form_state['merchant_type'] = $merchant_type;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_merchant_ui') . '/includes/commerce_merchant_ui.forms.inc';

  $form['#submit'][] = 'commerce_merchant_ui_merchant_type_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete the %name merchant type?', array('%name' => $merchant_type['name'])),
    'admin/commerce/merchants/types',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_merchant_merchant_type_delete_form().
 */
function commerce_merchant_ui_merchant_type_delete_form_submit($form, &$form_state) {
  $merchant_type = $form_state['merchant_type'];

  commerce_merchant_ui_merchant_type_delete($merchant_type['type']);

  drupal_set_message(t('The merchant type %name has been deleted.', array('%name' => $merchant_type['name'])));
  watchdog('commerce_merchant', 'Deleted merchant type %name.', array('%name' => $merchant_type['name']), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/commerce/merchants/types';
}
