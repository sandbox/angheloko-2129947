<?php

/**
 * @file
 * Page callbacks and form builder functions for administering merchants.
 */

/**
 * Menu callback: display a list of merchant types that the user can create.
 */
function commerce_merchant_ui_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);

  // Bypass the admin/commerce/merchants/add listing if only one merchant type
  // is available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }

  return theme('merchant_add_list', array('content' => $content));
}

/**
 * Displays the list of available merchant types for merchant creation.
 *
 * @ingroup themeable
 */
function theme_merchant_add_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="commerce-merchant-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer merchant types')) {
      $output = '<p>' . t('You have not created any merchant types yet. Go to the <a href="@create-merchant-type">merchant type creation page</a> to add a new merchant type.', array('@create-merchant-type' => url('admin/commerce/merchants/types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No merchant types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}

/**
 * Form callback wrapper: create or edit a merchant.
 *
 * @see commerce_merchant_merchant_form()
 */
function commerce_merchant_ui_merchant_form_wrapper($merchant) {
  // Include the forms file from the Merchant module.
  module_load_include('inc', 'commerce_merchant', 'includes/commerce_merchant.forms');
  return drupal_get_form('commerce_merchant_ui_merchant_form', $merchant);
}

/**
 * Form callback wrapper: confirmation form for deleting a merchant.
 *
 * @see commerce_merchant_merchant_delete_form()
 */
function commerce_merchant_ui_merchant_delete_form_wrapper($merchant) {
  // Include the forms file from the Merchant module.
  module_load_include('inc', 'commerce_merchant', 'includes/commerce_merchant.forms');
  return drupal_get_form('commerce_merchant_ui_merchant_delete_form', $merchant);
}
