<?php

/**
 * @file
 * Contains a views handler definition to display merchant titles in View using
 * merchant arguments.
 */

/**
 * Argument handler to display merchant titles in View using merchant arguments.
 */
class commerce_merchant_handler_argument_merchant_id extends views_handler_argument_numeric {
  function title_query() {
    $titles = array();
    $result = db_select('commerce_merchant', 'cm')
      ->fields('cm', array('name'))
      ->condition('cm.merchant_id', $this->value)
      ->execute();
    foreach ($result as $merchant) {
      $titles[] = check_plain($merchant->title);
    }
    return $titles;
  }
}
