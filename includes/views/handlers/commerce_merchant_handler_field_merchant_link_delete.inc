<?php

/**
 * @file
 * Contains the basic merchant delete link field handler.
 */

/**
 * Field handler to present a link to delete a merchant.
 */
class commerce_merchant_handler_field_merchant_link_delete extends commerce_merchant_handler_field_merchant_link {
  function construct() {
    parent::construct();

    $this->additional_fields['type'] = 'type';
    $this->additional_fields['uid'] = 'uid';
  }

  function render($values) {
    // Ensure the user has access to delete this merchant.
    $merchant = commerce_merchant_new();
    $merchant->merchant_id = $this->get_value($values, 'merchant_id');
    $merchant->type = $this->get_value($values, 'type');
    $merchant->uid = $this->get_value($values, 'uid');

    if (!commerce_merchant_access('delete', $merchant)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');

    return l($text, 'admin/commerce/merchants/' . $merchant->merchant_id . '/delete', array('query' => drupal_get_destination()));
  }
}
