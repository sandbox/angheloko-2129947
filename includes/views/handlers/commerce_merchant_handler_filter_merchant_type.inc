<?php

/**
 * @file
 * Contains the basic merchant type filter handler.
 */

/**
 * Filter by merchant type.
 */
class commerce_merchant_handler_filter_merchant_type extends views_handler_filter_in_operator {
  // Display a list of merchant types in the filter's options.
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Merchant type');
      $this->value_options = commerce_merchant_type_get_name();
    }
  }
}
