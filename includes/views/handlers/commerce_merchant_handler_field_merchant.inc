<?php

/**
 * @file
 * Contains the basic merchant field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a merchant.
 */
class commerce_merchant_handler_field_merchant extends views_handler_field {
  function init(&$view, &$options) {
    parent::init($view, $options);

    if (!empty($this->options['link_to_merchant'])) {
      $this->additional_fields['merchant_id'] = 'merchant_id';
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_merchant'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Provide the link to merchant option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_to_merchant'] = array(
      '#title' => t("Link this field to the merchant's administrative view page"),
      '#description' => t('This will override any other link you have set.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_merchant']),
    );
  }

  /**
   * Render whatever the data is as a link to the merchant.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_merchant']) && $data !== NULL && $data !== '') {
      $merchant_id = $this->get_value($values, 'merchant_id');
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = 'admin/commerce/merchants/' . $merchant_id;
    }

    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }
}
