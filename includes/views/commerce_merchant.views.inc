<?php

/**
 * @file
 * Export Drupal Commerce merchants to Views.
 */

/**
 * Implements hook_views_data().
 */
function commerce_merchant_views_data() {
  $data = array();

  $data['commerce_merchant']['table']['group']  = t('Commerce Merchant');

  $data['commerce_merchant']['table']['base'] = array(
    'field' => 'merchant_id',
    'title' => t('Commerce Merchant'),
    'help' => t('Merchants for Commerce.'),
    'access query tag' => 'commerce_merchant_access',
  );
  $data['commerce_merchant']['table']['entity type'] = 'commerce_merchant';

  // Expose the merchant ID.
  $data['commerce_merchant']['merchant_id'] = array(
    'title' => t('Merchant ID'),
    'help' => t('The unique internal identifier of the merchant.'),
    'field' => array(
      'handler' => 'commerce_merchant_handler_field_merchant',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'commerce_merchant_handler_argument_merchant_id',
    ),
  );

  // Expose the merchant type.
  $data['commerce_merchant']['type'] = array(
    'title' => t('Type'),
    'help' => t('The human-readable name of the type of the merchant.'),
    'field' => array(
      'handler' => 'commerce_merchant_handler_field_merchant_type',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'commerce_merchant_handler_filter_merchant_type',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the merchant name.
  $data['commerce_merchant']['name'] = array(
    'title' => t('Name'),
    'help' => t('The name of the merchant.'),
    'field' => array(
      'handler' => 'commerce_merchant_handler_field_merchant',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the creator uid.
  $data['commerce_merchant']['uid'] = array(
    'title' => t('Creator'),
    'help' => t('Relate a merchant to the user who created it.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('Merchant creator'),
    ),
  );

  // Expose the merchant status.
  $data['commerce_merchant']['status'] = array(
    'title' => t('Status'),
    'help' => t('Whether or not the merchant is active.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'active-disabled' => array(t('Active'), t('Disabled')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the created and changed timestamps.
  $data['commerce_merchant']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the merchant was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['commerce_merchant']['created_fulldate'] = array(
    'title' => t('Created date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['commerce_merchant']['created_year_month'] = array(
    'title' => t('Created year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['commerce_merchant']['created_timestamp_year'] = array(
    'title' => t('Created year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['commerce_merchant']['created_month'] = array(
    'title' => t('Created month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['commerce_merchant']['created_day'] = array(
    'title' => t('Created day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['commerce_merchant']['created_week'] = array(
    'title' => t('Created week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );

  $data['commerce_merchant']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the merchant was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['commerce_merchant']['changed_fulldate'] = array(
    'title' => t('Updated date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['commerce_merchant']['changed_year_month'] = array(
    'title' => t('Updated year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['commerce_merchant']['changed_timestamp_year'] = array(
    'title' => t('Updated year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['commerce_merchant']['changed_month'] = array(
    'title' => t('Updated month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['commerce_merchant']['changed_day'] = array(
    'title' => t('Updated day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['commerce_merchant']['changed_week'] = array(
    'title' => t('Updated week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );

  // Expose links to operate on the merchant.
  $data['commerce_merchant']['view_merchant'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a simple link to the administrator view of the merchant.'),
      'handler' => 'commerce_merchant_handler_field_merchant_link',
    ),
  );
  $data['commerce_merchant']['edit_merchant'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the merchant.'),
      'handler' => 'commerce_merchant_handler_field_merchant_link_edit',
    ),
  );
  $data['commerce_merchant']['delete_merchant'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the merchant.'),
      'handler' => 'commerce_merchant_handler_field_merchant_link_delete',
    ),
  );

  $data['commerce_merchant']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the merchant.'),
      'handler' => 'commerce_merchant_handler_field_merchant_operations',
    ),
  );

  $data['commerce_merchant']['empty_text'] = array(
    'title' => t('Empty text'),
    'help' => t('Displays an appropriate empty text message for merchant lists.'),
    'area' => array(
      'handler' => 'commerce_merchant_handler_area_empty_text',
    ),
  );

  return $data;
}
