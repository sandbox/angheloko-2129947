<?php

/**
 * @file
 * The controller for the merchant entity containing the CRUD operations.
 */

/**
 * The controller class for merchants contains methods for the merchant CRUD
 * operations.
 *
 * Mainly relies on the EntityAPIController class provided by the Entity
 * module, just overrides specific features.
 */
class CommerceMerchantEntityController extends DrupalCommerceEntityController {

  /**
   * Create a default merchant.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *
   * @return object
   *   A merchant object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $values += array(
      'merchant_id' => NULL,
      'is_new' => TRUE,
      'revision_id' => NULL,
      'name' => '',
      'uid' => '',
      'status' => 1,
      'created' => '',
      'changed' => '',
    );

    return parent::create($values);
  }

  /**
   * Saves a merchant.
   *
   * @param object $merchant
   *   The full merchant object to save.
   * @param object $transaction
   *   An optional transaction object.
   *
   * @return int
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($merchant, DatabaseTransaction $transaction = NULL) {
    global $user;

    // Hardcode the changed time.
    $merchant->changed = REQUEST_TIME;

    if (empty($merchant->{$this->idKey}) || !empty($merchant->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($merchant->created)) {
        $merchant->created = REQUEST_TIME;
      }
    }
    else {
      // If the merchant is not new but comes from an entity_create()
      // or similar function call that initializes the created timestamp and uid
      // value to empty strings, unset them to prevent destroying existing data
      // in those properties on update.
      if ($merchant->created === '') {
        unset($merchant->created);
      }
      if ($merchant->uid === '') {
        unset($merchant->uid);
      }
    }

    $merchant->revision_timestamp = REQUEST_TIME;
    $merchant->revision_uid = $user->uid;

    // Determine if we will be inserting a new merchant.
    $merchant->is_new = empty($merchant->merchant_id);

    if ($merchant->is_new || !empty($merchant->revision)) {
      // When inserting either a new merchant or revision, $entity->log must be
      // set because {commerce_merchant_revision}.log is a text column and
      // therefore cannot have a default value. However, it might not be set at
      // this point, so we ensure that it is at least an empty string in that
      // case.
      if (!isset($merchant->log)) {
        $merchant->log = '';
      }
    }
    elseif (empty($merchant->log)) {
      // If we are updating an existing merchant without adding a new revision,
      // we need to make sure $entity->log is unset whenever it is empty. As
      // long as $entity->log is unset, drupal_write_record() will not attempt
      // to update the existing database column when re-saving the revision.
      unset($merchant->log);
    }

    return parent::save($merchant, $transaction);
  }

  /**
   * Unserializes the data property of loaded merchants.
   */
  public function attachLoad(&$queried_merchants, $revision_id = FALSE) {
    foreach ($queried_merchants as $merchant_id => &$merchant) {
      $merchant->data = unserialize($merchant->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_commerce_merchant_load().
    parent::attachLoad($queried_merchants, $revision_id);
  }

  /**
   * Deletes multiple merchants by ID.
   *
   * @param array $merchant_ids
   *   An array of merchant IDs to delete.
   * @param object $transaction
   *   An optional transaction object.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($merchant_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($merchant_ids)) {
      $merchants = $this->load($merchant_ids, array());

      // Ensure the merchants can actually be deleted.
      foreach ((array) $merchants as $merchant_id => $merchant) {
        if (!commerce_merchant_can_delete($merchant)) {
          unset($merchants[$merchant_id]);
        }
      }

      // If none of the specified merchants can be deleted, return FALSE.
      if (empty($merchants)) {
        return FALSE;
      }

      parent::delete(array_keys($merchants), $transaction);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param object $merchant
   *   A merchant object.
   * @param string $view_mode
   *   View mode, e.g. 'full', 'teaser'...
   * @param string $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   *
   * @return array
   *   The renderable array.
   */
  public function buildContent($merchant, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Prepare a reusable array representing the CSS file to attach to the view.
    $attached = array(
      'css' => array(drupal_get_path('module', 'commerce_merchant') . '/theme/commerce_merchant.theme.css'),
    );

    // Add the default fields inherent to the merchant entity.
    $content['name'] = array(
      '#markup' => theme('commerce_merchant_name', array(
        'name' => $merchant->name,
        'label' => t('Name:'),
        'merchant' => $merchant)
      ),
      '#attached' => $attached,
    );
    $content['status'] = array(
      '#markup' => theme('commerce_merchant_status', array(
        'status' => $merchant->status,
        'label' => t('Status:'),
        'merchant' => $merchant)
      ),
      '#attached' => $attached,
    );

    return parent::buildContent($merchant, $view_mode, $langcode, $content);
  }
}
