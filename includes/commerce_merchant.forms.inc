<?php

/**
 * @file
 * Forms for creating, editing, and deleting merchants.
 */

/**
 * Form callback: create or edit a merchant.
 *
 * @param object $merchant
 *   The merchant object to edit or for a create form an empty merchant object
 *   with only a merchant type defined.
 */
function commerce_merchant_merchant_form($form, &$form_state, $merchant) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_merchant') . '/includes/commerce_merchant.forms.inc';

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $merchant->name,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  // Add the field related form elements.
  $form_state['commerce_merchant'] = $merchant;

  field_attach_form('commerce_merchant', $merchant, $form, $form_state);

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => array(
      '1' => t('Active'),
      '0' => t('Disabled'),
    ),
    '#default_value' => $merchant->status,
    '#required' => TRUE,
    '#weight' => 35,
  );

  // Load the merchant type to get the default revision setting.
  $merchant_type = commerce_merchant_type_load($merchant->type);

  // When updating a merchant, do not collapse the Change History fieldset if
  // the merchant type is configured to create a new revision by default.
  $form['change_history'] = array(
    '#type' => 'fieldset',
    '#title' => t('Change history'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($merchant->merchant_id) || empty($merchant_type['revision']),
    '#weight' => 50,
  );
  if (!empty($merchant->merchant_id)) {
    $form['change_history']['revision'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create new revision on update'),
      '#description' => t('If an update log message is entered, a revision will be created even if this is unchecked.'),
      '#default_value' => $merchant_type['revision'],
      '#access' => user_access('administer commerce_merchant entities'),
    );
  }
  $form['change_history']['log'] = array(
    '#type' => 'textarea',
    '#title' => !empty($merchant->merchant_id) ? t('Update log message') : t('Creation log message'),
    '#rows' => 4,
    '#description' => t('Provide an explanation of the changes you are making. This will provide a meaningful history of changes to this merchant.'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save merchant'),
    '#submit' => array_merge($submit, array('commerce_merchant_merchant_form_submit')),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'commerce_merchant_merchant_form_validate';

  return $form;
}

/**
 * Validation callback for commerce_merchant_merchant_form().
 */
function commerce_merchant_merchant_form_validate($form, &$form_state) {
  $merchant = $form_state['commerce_merchant'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('commerce_merchant', $merchant, $form, $form_state);
}

/**
 * Submit callback for commerce_merchant_merchant_form().
 */
function commerce_merchant_merchant_form_submit($form, &$form_state) {
  global $user;

  $merchant = &$form_state['commerce_merchant'];

  // Save default parameters back into the $merchant object.
  $merchant->name = $form_state['values']['name'];
  $merchant->status = $form_state['values']['status'];

  // Set the merchant's uid if it's being created at this time.
  if (empty($merchant->merchant_id)) {
    $merchant->uid = $user->uid;
  }

  // Trigger a new revision if the checkbox was enabled or a log message
  // supplied.
  if ((user_access('administer commerce_merchant entities') && !empty($form_state['values']['revision'])) ||
    (!user_access('administer commerce_merchant entities') && !empty($form['change_history']['revision']['#default_value'])) ||
    !empty($form_state['values']['log'])) {
    $merchant->revision = TRUE;
    $merchant->log = $form_state['values']['log'];
  }

  // Notify field widgets.
  field_attach_submit('commerce_merchant', $merchant, $form, $form_state);

  // Save the merchant.
  commerce_merchant_save($merchant);

  // Redirect based on the button clicked.
  drupal_set_message(t('Merchant saved.'));
}

/**
 * Form callback: confirmation form for deleting a merchant.
 *
 * @param object $merchant
 *   The merchant object to be deleted.
 *
 * @see confirm_form()
 */
function commerce_merchant_merchant_delete_form($form, &$form_state, $merchant) {
  $form_state['merchant'] = $merchant;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_merchant') . '/includes/commerce_merchant.forms.inc';

  $form['#submit'][] = 'commerce_merchant_merchant_delete_form_submit';

  $content = entity_view('commerce_merchant', array($merchant->merchant_id => $merchant));

  $form = confirm_form($form,
    t('Are you sure you want to delete %name?', array('%name' => $merchant->name)),
    '',
    drupal_render($content) . '<p>' . t('Deleting this merchant cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_merchant_merchant_delete_form().
 */
function commerce_merchant_merchant_delete_form_submit($form, &$form_state) {
  $merchant = $form_state['merchant'];

  if (commerce_merchant_delete($merchant->merchant_id)) {
    drupal_set_message(t('%name has been deleted.', array('%name' => $merchant->name)));
    watchdog('commerce_merchant', 'Deleted merchant %name.', array('%name' => $merchant->name), WATCHDOG_NOTICE);
  }
  else {
    drupal_set_message(t('%name could not be deleted.', array('%name' => $merchant->name)), 'error');
  }
}
