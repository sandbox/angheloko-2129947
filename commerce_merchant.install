<?php

/**
 * @file
 * Contains install hooks.
 */

/**
 * Implements hook_schema().
 */
function commerce_merchant_schema() {
  $schema = array();

  $schema['commerce_merchant'] = array(
    'description' => 'The base table for merchants.',
    'fields' => array(
      'merchant_id' => array(
        'description' => 'The primary identifier for a merchant, used internally only.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'revision_id' => array(
        'description' => 'The current {commerce_merchant_revision}.revision_id version identifier.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'name' => array(
        'description' => 'The name of this merchant, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'The {commerce_merchant_type}.type of this merchant.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'The {users}.uid that created this merchant.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether or not the merchant is active or not.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the merchant was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the merchant was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data.',
      ),
    ),
    'primary key' => array('merchant_id'),
    'indexes' => array(
      'merchant_type' => array('type'),
      'uid' => array('uid'),
    ),
    'unique keys' => array(
      'revision_id' => array('revision_id'),
    ),
    'foreign keys' => array(
      'current_revision' => array(
        'table' => 'commerce_merchant_revision',
        'columns' => array('revision_id' => 'revision_id'),
      ),
      'creator' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
  );

  $schema['commerce_merchant_revision'] = array(
    'description' => 'Saves information about each saved revision of a {commerce_merchant}.',
    'fields' => array(
      'merchant_id' => array(
        'description' => 'The {commerce_merchant}.merchant_id of the merchant this revision belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'revision_id' => array(
        'description' => 'The primary identifier for this revision.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The name of this merchant for this revision',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'status' => array(
        'description' => 'The status of this revision.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
      'log' => array(
        'description' => 'The log entry explaining the changes in this version.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big',
      ),
      'revision_uid' => array(
        'description' => 'The {users}.uid that created this revision.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'revision_timestamp' => array(
        'description' => 'The Unix timestamp when this revision was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data for this revision.',
      ),
    ),
    'primary key' => array('revision_id'),
    'indexes' => array(
      'merchant_id' => array('merchant_id'),
      'revision_uid' => array('revision_uid'),
    ),
    'foreign keys' => array(
      'merchant' => array(
        'table' => 'commerce_merchant',
        'columns' => array('merchant_id' => 'merchant_id'),
      ),
    ),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function commerce_merchant_uninstall() {
  // Delete any field instance attached to a merchant type.
  module_load_include('module', 'commerce');
  commerce_delete_instances('commerce_merchant');
}
