<?php

/**
 * @file
 * Contains page callbacks.
 */

/**
 * Page callback for /merchant.
 */
function commerce_merchant_user_merchant_page() {
  global $user;

  // Get the merchants that the current user is associated with.
  if ($merchants = commerce_merchant_user_load_merchant_by_user($user)) {
    // If user is only part of one merchant, bypass the merchant list.
    if (count($merchants) == 1) {
      $merchant = reset($merchants);
      drupal_goto('merchant/' . $merchant->merchant_id);
    }
    else {
      return theme('commerce_merchant_user_merchant_select_list', array('merchants' => $merchants));
    }
  }
  else {
    return MENU_ACCESS_DENIED;
  }
}

/**
 * Displays the list of available merchants associated to a given user.
 *
 * @ingroup themeable
 */
function theme_commerce_merchant_user_merchant_select_list($variables) {
  $merchants = $variables['merchants'];
  $output = '';

  $output = '<dl class="commerce-merchant-select-list">';
  foreach ($merchants as $merchant) {
    $output .= '<dt>' . l($merchant->name, 'merchant/' . $merchant->merchant_id) . '</dt>';
  }
  $output .= '</dl>';

  return $output;
}
