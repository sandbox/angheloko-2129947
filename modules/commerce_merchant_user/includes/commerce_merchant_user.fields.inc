<?php

/**
 * @file
 * Defines the user entityreference field structure.
 */

/**
 * Returns an array of essential fields to be attached to merchant bundles.
 */
function commerce_merchant_user_custom_fields() {
  $fields = array();

  $fields['commerce_merchant_user'] = array(
    'field_name' => 'commerce_merchant_user',
    'type' => 'entityreference',
    'cardinality' => '-1',
    'translatable' => FALSE,
    'locked' => TRUE,

    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(),
      ),
      'target_type' => 'user',
    ),
  );

  return $fields;
}

/**
 * Returns an array defining the field instances to be attache to merchant
 *   entities.
 */
function commerce_merchant_user_custom_field_instances() {
  $instances = array();

  $instances['commerce_merchant_user'] = array(
    'field_name' => 'commerce_merchant_user',
    'label' => 'Users',
    'required' => TRUE,
    'settings' => array(),

    'widget' => array(
      'active' => 1,
      'settings' => array(
        'match_operator' => 'STARTS_WITH',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
    ),

    'display' => array(),
  );

  return $instances;
}
