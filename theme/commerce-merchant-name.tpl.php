<?php

/**
 * @file
 * Default theme implementation to present the name on a merchant page.
 *
 * Available variables:
 * - $name: The name to render.
 * - $label: If present, the string to use as the name label.
 *
 * Helper variables:
 * - $merchant: The fully loaded merchant object the name belongs to.
 */
?>
<?php if ($name): ?>
  <div class="commerce-merchant-name">
    <?php if ($label): ?>
      <div class="commerce-merchant-name-label">
        <?php print $label; ?>
      </div>
    <?php endif; ?>
    <?php print $name; ?>
  </div>
<?php endif; ?>
