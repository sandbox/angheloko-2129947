<?php

/**
 * @file
 * Default theme implementation to present the status on a merchant page.
 *
 * Available variables:
 * - $status: The string representation of a merchant's status to render.
 * - $label: If present, the string to use as the status label.
 *
 * Helper variables:
 * - $merchant: The fully loaded merchant object the status belongs to.
 */
?>
<?php if ($status): ?>
  <div class="commerce-merchant-status">
    <?php if ($label): ?>
      <div class="commerce-merchant-status-label">
        <?php print $label; ?>
      </div>
    <?php endif; ?>
    <?php print $status; ?>
  </div>
<?php endif; ?>
