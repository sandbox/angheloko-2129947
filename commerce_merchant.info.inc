<?php

/**
 * @file
 * Provides metadata for the merchant entity.
 */

/**
 * Implements hook_entity_property_info().
 */
function commerce_merchant_entity_property_info() {
  $info = array();

  // Add meta-data about the basic commerce_merchant properties.
  $properties = &$info['commerce_merchant']['properties'];

  $properties['merchant_id'] = array(
    'label' => t('Merchant ID'),
    'description' => t('The internal numeric ID of the merchant.'),
    'type' => 'integer',
    'schema field' => 'merchant_id',
  );

  $properties['type'] = array(
    'label' => t('Type'),
    'description' => t('The type of the merchant.'),
    'type' => 'token',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_merchant entities',
    'options list' => 'commerce_merchant_type_options_list',
    'required' => TRUE,
    'schema field' => 'type',
  );

  $properties['name'] = array(
    'label' => t('Name'),
    'description' => t('The name of the merchant.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'name',
  );

  $properties['status'] = array(
    'label' => t('Status'),
    'description' => t('Boolean indicating whether the merchant is active or disabled.'),
    'type' => 'boolean',
    'options list' => 'commerce_merchant_status_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_merchant entities',
    'schema field' => 'status',
  );

  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the merchant was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_merchant entities',
    'schema field' => 'created',
  );

  $properties['changed'] = array(
    'label' => t('Date updated'),
    'description' => t('The date the merchant was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'query callback' => 'entity_metadata_table_query',
    'setter permission' => 'administer commerce_merchant entities',
    'schema field' => 'changed',
  );

  $properties['uid'] = array(
    'label' => t('Creator ID'),
    'type' => 'integer',
    'description' => t('The unique ID of the merchant creator.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_merchant entities',
    'clear' => array('creator'),
    'schema field' => 'uid',
  );

  $properties['creator'] = array(
    'label' => t('Creator'),
    'type' => 'user',
    'description' => t('The creator of the merchant.'),
    'getter callback' => 'commerce_merchant_get_properties',
    'setter callback' => 'commerce_merchant_set_properties',
    'setter permission' => 'administer commerce_merchant entities',
    'required' => TRUE,
    'computed' => TRUE,
    'clear' => array('uid'),
  );

  return $info;
}
