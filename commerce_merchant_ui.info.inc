<?php

/**
 * @file
 * Contains alterations to the merchant entity property info.
 */

/**
 * Implements hook_entity_property_info_alter().
 *
 * Add UI bound merchant properties.
 */
function commerce_merchant_ui_entity_property_info_alter(&$info) {
  $info['commerce_merchant']['properties']['edit_url'] = array(
    'label' => t('Edit URL'),
    'description' => t("The URL of the merchant's edit page."),
    'getter callback' => 'commerce_merchant_get_properties',
    'type' => 'uri',
  );
}
